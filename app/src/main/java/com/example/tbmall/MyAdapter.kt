package com.example.tbmall

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row.view.*

class MyAdapter(val arrayList: MutableList<Model>, val context: Context) :
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var model: Model

        @SuppressLint("SetTextI18n")
        fun bindItems(){


            model = arrayList[adapterPosition]
            itemView.titleTv.text = model.title
            itemView.descriptionTv.text = "Floor : " + model.des
            Glide.with(context).load(model.image).into(itemView.imageIv)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)

        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems()

        holder.itemView.setOnClickListener{


            val model = arrayList[position]

            val title = model.title
            val floor = "Floor : " + model.des
            val image = model.image

            val intent = Intent(context, MapActivity::class.java)

            intent.putExtra("extraTitle", title)
            intent.putExtra("extraFloor", floor)
            intent.putExtra("extraImage", image)

            context.startActivity(intent)
        }


    }

    override fun getItemCount() = arrayList.size
}