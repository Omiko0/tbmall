package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.actuation.Animate
import com.aldebaran.qi.sdk.`object`.actuation.Animation
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.`object`.humanawareness.HumanAwareness
import com.aldebaran.qi.sdk.builder.*
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_food.*
import kotlinx.android.synthetic.main.activity_stores.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.log

class StoresActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object{
        var searchStoreName: String = ""

        var storeArrayList = mutableListOf<Model>()

        var titleStore = ""
        var floorStore = ""
        var imageStore = ""

    }

    lateinit var storeAdapter: MyAdapter

    var displayStoreArrayList = mutableListOf<Model>()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stores)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

        storeAdapter = MyAdapter(displayStoreArrayList, this)

        recyclerViewStore.layoutManager = GridLayoutManager(this, 3)
        recyclerViewStore.adapter = storeAdapter

    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private var storeNames = mutableListOf<Phrase>()


    private fun addInTopic(chatbot: QiChatbot){
        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                storeNames.clear()


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "dressing") {

                        storeNames.add(Phrase(i.child("name").value.toString()))


                    }

                }

                add()


            }

            private fun add(){

                val thread = Thread(Runnable {


                    val editablePhraseSet = chatbot.dynamicConcept("Name")


                    editablePhraseSet!!.addPhrases(storeNames)
                    Log.i("abavnaxot", storeNames.toString())


                })

                thread.start()
            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })


    }



    private fun uploadStore(){


        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "dressing"){

                        storeArrayList.add(Model(i.child("name").value.toString(),
                            i.child("floor").value.toString(),
                            i.child("image").value.toString(),
                            i.child("product1").value.toString(),
                            i.child("product2").value.toString(),
                            i.child("product3").value.toString()))

                    }

                    displayStoreArrayList.clear()

                    displayStoreArrayList.addAll(storeArrayList)

                }

                storeAdapter.notifyDataSetChanged()
                recyclerViewStore.scrollToPosition(displayStoreArrayList.size - 1)


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })


    }


    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(
        qiContext
    ) {


        override fun runWith(params: List<String>) {

            titleStore = ""
            floorStore = ""
            imageStore = ""


            if (params.size > 0) {

                searchStoreName = params[0]

                storeArrayList.forEach {

                    if (searchStoreName == it.title){

                        titleStore = it.title
                        floorStore = it.des
                        imageStore = it.image

                    }
                }


            }

        }


        override fun stop() {

        }

    }

    override fun onRobotFocusGained(qiContext: QiContext?) {


        backButtonToolbar.setOnClickListener{

            displayStoreArrayList.clear()

            storeArrayList.clear()

            val back = Intent(this, ShoppingActivity::class.java)
            startActivity(back)
        }

        uploadStore()




        searchButtonToolbar.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {

                if (p0!!.isNotEmpty()) {
                    displayStoreArrayList.clear()


                    val search = p0.toLowerCase(Locale.getDefault())

                    storeArrayList.forEach {

                        if (it.title.toLowerCase(Locale.getDefault()).contains(search)) {

                            displayStoreArrayList.add(it)

                        }
                    }

                    recyclerViewStore.adapter!!.notifyDataSetChanged()

                }

                else {

                    displayStoreArrayList.clear()
                    displayStoreArrayList.addAll(storeArrayList)

                    recyclerViewStore.adapter!!.notifyDataSetChanged()
                }

                return true
            }


        })


        val phrase: Phrase = Phrase("tell me witch store would you like to go? or press the button.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.store)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        addInTopic(chatbot)




        val executors = HashMap<String, QiChatExecutor>()

        executors["store"] = MyQiChatExecutor(qiContext)

        chatbot.executors = executors


        val chatbots = mutableListOf<Chatbot>()
        chatbots.add(chatbot)


        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            Log.i("TAG", "bookmark")


            if (bookmark.name == "goBack") {

                storeArrayList.clear()

                val back = Intent(this, ShoppingActivity::class.java)
                startActivity(back)
            }

            if (bookmark.name == "showFloor") {

                val myAnimation: Animation = AnimationBuilder.with(qiContext)
                    .withResources(R.raw.show_self_a001)
                    .build()

                val animate: Animate = AnimateBuilder.with(qiContext)
                    .withAnimation(myAnimation)
                    .build()

                animate.async().run()


                if (bookmark.name == "showFloor") {

                    val mapActivityDirection = Intent(this, MapActivity::class.java)
                    mapActivityDirection.putExtra("extraTitle", titleStore)
                    mapActivityDirection.putExtra("extraFloor", floorStore)
                    mapActivityDirection.putExtra("extraImage", imageStore)
                    startActivity(mapActivityDirection)


                }
            }
        }


        chat.async().run()

    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {
    }
}