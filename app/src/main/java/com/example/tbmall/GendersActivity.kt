package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_ages.*
import kotlinx.android.synthetic.main.activity_genders.*
import kotlinx.android.synthetic.main.simpletoolbar.*

class GendersActivity : RobotActivity(), RobotLifecycleCallbacks {

        var male: Int = 0
        var female: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genders)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }


    private fun chart(dataList: MutableList<String>){


        for (each in dataList){

            if (each == "MALE"){

                male += 1
            }
            if (each == "FEMALE"){

                female += 1

            }
        }

        val pie = AnyChart.pie()


        val data: MutableList<DataEntry> = ArrayList()
        data.add(ValueDataEntry("MALE", male))
        data.add(ValueDataEntry("FEMALE", female))

        pie.data(data)

        chart_view.setChart(pie)
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {

        backButtonSimpleToolbar.setOnClickListener{


            val menu = Intent(this, StatisticActivity::class.java)

            startActivity(menu)


        }

        val phrase: Phrase = Phrase("Please wait, i'm collecting all the data to show you.In a few second you will see gender statistic. If you want to see the value, you can press the chart")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.genders)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()


        val ref = FirebaseDatabase.getInstance().getReference("TBData")

        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                val genderList = mutableListOf<String>()

                for (i in p0.children) {

                    val gender = i.child("gender").value.toString()

                    genderList.add(gender)

                }

                chart(genderList)

            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })

        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "goBack") {

                val menu = Intent(this, StatisticActivity::class.java)
                startActivity(menu)


            }

        }

        chat.async().run()

    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}