package com.example.tbmall

import android.app.Person
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.aldebaran.qi.TypeToken
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_shopping.*
import kotlinx.android.synthetic.main.simpletoolbar.*
import kotlin.concurrent.thread

class ShoppingActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object{

        lateinit var choosenThing : String

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)


    }




    private fun storesActivity(){

        val store = Intent(this, StoresActivity::class.java)

        startActivity(store)

    }
    private fun foodActivity(){


        val food = Intent(this, FoodActivity::class.java)

        startActivity(food)

    }
    private fun servicesActivity(){

        val services = Intent(this, ServicesActivity::class.java)

        startActivity(services)

    }

    private fun menu(){

        val menu = Intent(this, MainActivity::class.java)

        startActivity(menu)

    }

    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(qiContext) {


        override fun runWith(params: List<String>) {
            Log.i("TAG", "runWith")


            if (params.size > 0) {

                choosenThing = params[0]

                Log.i("159", choosenThing)

            }

        }


        override fun stop() {
            // This is called when chat is canceled or stopped.
            Log.i("TAG", "QiChatExecutor stopped")
        }

    }

    override fun onRobotFocusGained(qiContext: QiContext?) {

        backButtonSimpleToolbar.setOnClickListener{



            menu()

        }

        servicesBtn.setOnClickListener{


            servicesActivity()

        }

        foodBtn.setOnClickListener{


            foodActivity()


        }

        storesBtn.setOnClickListener{



            storesActivity()


        }

        val phrase: Phrase = Phrase("What are you looking for? Dressing stores? fast foods? or services?")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.shopping)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        val executors = HashMap<String, QiChatExecutor>()

        executors["want"] = MyQiChatExecutor(qiContext)

        chatbot.executors = executors


        val chatbots = mutableListOf<Chatbot>()
        chatbots.add(chatbot)

        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            Log.i("TAG", "bookmark")

            if (bookmark.name == "goToChoosen") {

                if (choosenThing == "stores"){

                    storesActivity()

                }

                if (choosenThing == "fastfood"){

                    val services = Intent(this, FoodActivity::class.java)

                    startActivity(services)



                }
                if (choosenThing == "services"){

                    servicesActivity()

                }

            }

            if (bookmark.name == "back"){

                val intent = Intent(this, MainActivity::class.java)

                startActivity(intent)
            }




        }

        chat.async().run()



    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}