package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_ages.*
import kotlinx.android.synthetic.main.activity_genders.*
import kotlinx.android.synthetic.main.simpletoolbar.*


class AgesActivity : RobotActivity(), RobotLifecycleCallbacks {
    var until18 = 0
    var until30 = 0
    var more30 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ages)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)


    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private fun chart(ageList: MutableList<Int>){

        for (each in ageList){

            if (each <= 18){

                until18 += 1

            }
            if (each in 20..30){

                until30 += 1

            }
            if (each > 30){

                more30 += 1

            }

        }

        val pie = AnyChart.pie()

        val data: MutableList<DataEntry> = ArrayList()
        data.add(ValueDataEntry("UNTIL 18", until18))
        data.add(ValueDataEntry("UNTIL 60", until30))
        data.add(ValueDataEntry("MORE 60", more30))

        pie.data(data)

        any_chart_view.setChart(pie)
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {

        backButtonSimpleToolbar.setOnClickListener{


            val menu = Intent(this, StatisticActivity::class.java)

            startActivity(menu)


        }

        val phrase: Phrase = Phrase("Please wait, i'm collecting all the data to show you.   In a few second you will see ages statistic. If you want to see the value, you can press the chart")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.ages)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()


        val ref = FirebaseDatabase.getInstance().getReference("TBData")

        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                val ageList = mutableListOf<Int>()

                for (i in p0.children) {

                    val age = i.child("age").value.toString()

                    ageList.add(age.toInt())

                }


                chart(ageList)

            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })

        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            Log.i("TAG", "bookmark")

            if (bookmark.name == "goBack") {

                val menu = Intent(this, StatisticActivity::class.java)
                startActivity(menu)


            }

        }

        chat.async().run()

    }

    override fun onRobotFocusLost() {



    }

    override fun onRobotFocusRefused(reason: String?) {



    }

}