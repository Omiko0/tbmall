package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_food.*
import kotlinx.android.synthetic.main.activity_services.*
import kotlinx.android.synthetic.main.activity_stores.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*
import kotlin.collections.HashMap

class ServicesActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object{
        var servicesName: String = ""

        var servicesArrayList = mutableListOf<Model>()

        var titleServices = ""
        var floorServices = ""
        var imageServices = ""

    }

    lateinit var servicesAdapter: MyAdapter

    var displayServicesArrayList = mutableListOf<Model>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)


        servicesAdapter = MyAdapter(displayServicesArrayList, this)

        recyclerViewServices.layoutManager = GridLayoutManager(this, 3)
        recyclerViewServices.adapter = servicesAdapter


    }



    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private var servicesStoreNames = mutableListOf<Phrase>()


    private fun addInServicesTopic(chatbot: QiChatbot){
        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                servicesStoreNames.clear()


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "service") {


                        servicesStoreNames.add(Phrase(i.child("name").value.toString()))


                    }

                }

                val thread = Thread(Runnable {


                    val editablePhraseSet = chatbot.dynamicConcept("servicesName")



                    editablePhraseSet!!.addPhrases(servicesStoreNames)

                })

                thread.start()

                Log.i("servicesName", servicesStoreNames.toString())


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })


    }


    private fun uploadServices(){


        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "service"){

                        servicesArrayList.add(Model(i.child("name").value.toString(),
                            i.child("floor").value.toString(),
                            i.child("image").value.toString(),
                            i.child("product1").value.toString(),
                            i.child("product2").value.toString(),
                            i.child("product3").value.toString()))

                    }

                    displayServicesArrayList.clear()
                    displayServicesArrayList.addAll(servicesArrayList)


                    servicesAdapter.notifyDataSetChanged()
                    recyclerViewServices.scrollToPosition(displayServicesArrayList.size - 1)

                }


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })
    }



    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(
        qiContext
    ) {




        override fun runWith(params: List<String>) {



            titleServices = ""
            floorServices = ""
            imageServices = ""

            if (params.size > 0) {

                servicesName = params[0]


                servicesArrayList.forEach {

                    if (servicesName == it.title){

                        titleServices = it.title
                        floorServices = it.des
                        imageServices = it.image

                    }
                }


            }

        }


        override fun stop() {

        }


    }

    override fun onRobotFocusGained(qiContext: QiContext?) {

        backButtonToolbar.setOnClickListener{

            servicesArrayList.clear()

            val intent = Intent(this, ShoppingActivity::class.java)

            startActivity(intent)
        }

        uploadServices()

        searchButtonToolbar.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {

                if (p0!!.isNotEmpty()) {
                    displayServicesArrayList.clear()


                    val search = p0.toLowerCase(Locale.getDefault())

                    servicesArrayList.forEach {

                        if (it.title.toLowerCase(Locale.getDefault()).contains(search)) {

                            displayServicesArrayList.add(it)

                        }
                    }

                    recyclerViewServices.adapter!!.notifyDataSetChanged()

                }

                else {

                    displayServicesArrayList.clear()
                    displayServicesArrayList.addAll(servicesArrayList)

                    recyclerViewServices.adapter!!.notifyDataSetChanged()
                }

                return true
            }


        })


        val phrase: Phrase = Phrase("tell me where do you want to go? or press the button.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()


        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.services)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        addInServicesTopic(chatbot)



        val executors = HashMap<String, QiChatExecutor>()

        executors["servicesName"] = MyQiChatExecutor(qiContext)

        chatbot.executors = executors


        val chatbots = mutableListOf<Chatbot>()
        chatbots.add(chatbot)


        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "goBack"){

                servicesArrayList.clear()

                val menu = Intent(this, ShoppingActivity::class.java)
                startActivity(menu)
            }

            if (bookmark.name == "showFloor"){

                val directToMapActivity = Intent(this, MapActivity::class.java)
                directToMapActivity.putExtra("extraTitle", titleServices)
                directToMapActivity.putExtra("extraFloor", floorServices)
                directToMapActivity.putExtra("extraImage", imageServices)
                startActivity(directToMapActivity)


            }
        }

        chat.async().run()

    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}