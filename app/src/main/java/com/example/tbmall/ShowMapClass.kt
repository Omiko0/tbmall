package com.example.tbmall

import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor

class ShowMapClass(qiContext: QiContext?) : BaseQiChatExecutor(qiContext) {


    override fun runWith(params: List<String>) {

        if (params.size > 0) {

            MenuActivity.selectedStore = params[0]

        }

    }


    override fun stop() {

    }

}