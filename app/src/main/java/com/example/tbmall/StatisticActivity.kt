package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.activity_statistic.*
import kotlinx.android.synthetic.main.simpletoolbar.*

class StatisticActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object {
        lateinit var chosenStatistic: String


    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

    }

    private fun agesStatistic(){


        val agesStatistic = Intent(this, AgesActivity::class.java)

        startActivity(agesStatistic)

    }

    private fun gendersStatistic(){

        val gendersStatistic = Intent(this, GendersActivity::class.java)

        startActivity(gendersStatistic)

    }




    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(
        qiContext
    ) {


        override fun runWith(params: List<String>) {
            Log.i("TAG", "runWith")

            Log.i("TAG", "runWit")



            if (params.size > 0) {

                chosenStatistic = params[0]


            }

        }


        override fun stop() {
            // This is called when chat is canceled or stopped.
            Log.i("TAG", "QiChatExecutor stopped")
        }


    }

    override fun onRobotFocusGained(qiContext: QiContext?) {

        agesBtn.setOnClickListener{

           agesStatistic()

        }

        gendersBtn.setOnClickListener{

            gendersStatistic()
        }
        backButtonSimpleToolbar.setOnClickListener{

            val back = Intent(this, MainActivity::class.java)
            startActivity(back)

        }

        val phrase: Phrase = Phrase("There you can choose witch statistic of customer you want to see, age or gender.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.statistic)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        val executors = HashMap<String, QiChatExecutor>()

        // Map the executor name from the topic to our qiChatbotExecutor
        executors["chosen"] = MyQiChatExecutor(qiContext)

        // Set the executors to the qiChatbot
        chatbot.executors = executors


        val chatbots = mutableListOf<Chatbot>()
        chatbots.add(chatbot)

        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "chooseData") {

                if (chosenStatistic == "ages" || chosenStatistic == "age statistic"){
                    agesStatistic()

                }

                if (chosenStatistic == "genders" || chosenStatistic == "gender statistic"){
                    gendersStatistic()

                }

            }

            if(bookmark.name == "goBack"){

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)

            }


        }

        chat.async().run()

    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}