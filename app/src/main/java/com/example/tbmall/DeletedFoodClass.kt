package com.example.tbmall

import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor
import com.example.tbmall.SearchFoodActivity.Companion.deletedFoodName


class DeletedFoodClass(qiContext: QiContext?) : BaseQiChatExecutor(
    qiContext
) {


    override fun runWith(params: List<String>) {


        if (params.size > 0) {

            deletedFoodName = params[0]

            Log.i("chemikai", deletedFoodName)



        }


    }


    override fun stop() {

    }


}