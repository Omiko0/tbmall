package com.example.tbmall

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.`object`.humanawareness.HumanAwareness
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_food.*
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_stores.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*
import kotlin.collections.HashMap

class FoodActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object{
        var searchFoodStoreName: String = ""

        var foodArrayList = mutableListOf<Model>()

        var titleFood = ""
        var floorFood = ""
        var imageFood = ""

    }

    lateinit var adapterFood: MyAdapter


    var displayFodArrayList = mutableListOf<Model>()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

        adapterFood = MyAdapter(displayFodArrayList, this)

        recyclerViewFood.layoutManager = GridLayoutManager(this, 3)
        recyclerViewFood.adapter = adapterFood




    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private var foodStoreNames = mutableListOf<Phrase>()


    private fun addInFoodTopic(chatbot: QiChatbot){
        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                foodStoreNames.clear()


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "kveba") {


                        foodStoreNames.add(Phrase(i.child("name").value.toString()))


                    }

                }

                val thread = Thread(Runnable {


                    val editablePhraseSet = chatbot.dynamicConcept("foodName")



                    editablePhraseSet!!.addPhrases(foodStoreNames)

                })

                thread.start()

                Log.i("foodNames", foodStoreNames.toString())


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })


    }


    private fun uploadStore(){


        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                for (i in p0.children) {

                    if (i.child("type").value.toString() == "kveba"){

                        foodArrayList.add(Model(i.child("name").value.toString(),
                                                i.child("floor").value.toString(),
                                                i.child("image").value.toString(),
                                                i.child("product1").value.toString(),
                                                i.child("product2").value.toString(),
                                                i.child("product3").value.toString()))

                    }

                    displayFodArrayList.clear()

                    displayFodArrayList.addAll(foodArrayList)

                }

                adapterFood.notifyDataSetChanged()
                recyclerViewFood.scrollToPosition(displayFodArrayList.size - 1)

            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })

    }


    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(
        qiContext
    ) {

        override fun runWith(params: List<String>) {

            titleFood = ""
            floorFood = ""
            imageFood = ""


            if (params.size > 0) {

                searchFoodStoreName = params[0]

                foodArrayList.forEach {

                    if (searchFoodStoreName in it.title){

                        titleFood = it.title
                        floorFood = it.des
                        imageFood = it.image

                    }

                }

            }

        }


        override fun stop() {

        }


    }



    override fun onRobotFocusGained(qiContext: QiContext?) {


        backButtonToolbar.setOnClickListener{

            foodArrayList.clear()
            displayFodArrayList.clear()

            val intent = Intent(this, ShoppingActivity::class.java)
            startActivity(intent)
        }

        uploadStore()


        searchButtonToolbar.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {

                if (p0!!.isNotEmpty()) {
                    displayFodArrayList.clear()


                    val search = p0.toLowerCase(Locale.getDefault())

                    foodArrayList.forEach {

                        if (it.title.toLowerCase(Locale.getDefault()).contains(search)) {

                            displayFodArrayList.add(it)

                        }
                    }

                    recyclerViewFood.adapter!!.notifyDataSetChanged()

                }

                else {

                    displayFodArrayList.clear()
                    displayFodArrayList.addAll(foodArrayList)

                    recyclerViewFood.adapter!!.notifyDataSetChanged()
                }

                return true
            }


        })


        val phrase: Phrase = Phrase("tell me witch restaurant would you like to go? or press the button.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.food)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        addInFoodTopic(chatbot)




        val executors = HashMap<String, QiChatExecutor>()

        executors["foodStore"] = MyQiChatExecutor(qiContext)

        chatbot.executors = executors


        val chatbots = mutableListOf<Chatbot>()

        chatbots.add(chatbot)


        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "goBack"){

                foodArrayList.clear()
                displayFodArrayList.clear()


                val menu = Intent(this, ShoppingActivity::class.java)
                startActivity(menu)
                finish()
            }

            if (bookmark.name == "showFloor"){

                val directToMap = Intent(this, MapActivity::class.java)
                directToMap.putExtra("extraTitle", titleFood)
                directToMap.putExtra("extraFloor", floorFood)
                directToMap.putExtra("extraImage", imageFood)
                startActivity(directToMap)

            }
        }


        chat.async().run()

    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}