package com.example.tbmall

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Layout
import android.util.Log
import android.widget.CheckBox
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.bumptech.glide.Glide
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_stores.*
import kotlinx.android.synthetic.main.simpletoolbar.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.concurrent.thread

class MenuActivity : RobotActivity(), RobotLifecycleCallbacks{

    companion object{

        var selectedFood: String = ""
        var deletedFood: String = ""
        var selectedStore: String = ""

        var arrayList = mutableListOf<Model>()
        lateinit var myAdapter: MyAdapter

    }

    var nameList = mutableListOf<Model>()
    var productList = mutableListOf<Model>()


    var phrases = mutableListOf<Phrase>()

    var storeName = ""
    var floor = ""
    var price = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)



        myAdapter = MyAdapter(arrayList, this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = myAdapter



    }

    private fun add(chatbot: QiChatbot){

        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {


                for (i in p0.children) {


                    nameList.add(
                        Model(
                            i.child("name").value.toString(),
                            i.child("floor").value.toString(),
                            i.child("image").value.toString(),
                            i.child("product1").value.toString(),
                            i.child("product2").value.toString(),
                            i.child("product3").value.toString()
                        )
                    )




                }

                val thread = Thread(Runnable {

                    val editablePhraseSet = chatbot.dynamicConcept("storeName")

                    phrases.clear()

                    nameList.forEach {

                        Log.i("saxelebii", it.title)

                        phrases.add(Phrase(it.title))
                        phrases.add(Phrase(it.product1))
                        phrases.add(Phrase(it.product2))
                        phrases.add(Phrase(it.product3))

                    }

                    editablePhraseSet!!.addPhrases(phrases)
                    Log.i("achvene", phrases.toString())


                })

               thread.start()




            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })
    }


    private fun foodSelection(food: String, boolian: Boolean, change: String? = null) {


        val foodId = mapOf<String, CheckBox>(
            "Chicken Fries" to Chicken_Fries,
            "Dinner Baskets" to Dinner_Baskets,
            "Steak and Cheese" to Steak_and_Cheese,
            "philadelphia roll" to philadelphia_roll,
            "chicken sandwiches" to chicken_sandwiches,
            "Espresso" to Espresso
        )

        if (change == "delete") {


        for (each in foodId.keys) {

            if (each == food) {

                this.runOnUiThread {

                    Log.i("foodID", each)


                    foodId[each]?.isChecked = boolian


                }


            }


        }

    }else{

            for (each in foodId.keys) {

                if (each == food && foodId[each]?.isChecked == false) {

                    this.runOnUiThread {

                        Log.i("foodID", each)


                        foodId[each]?.isChecked = boolian
                        searchFood(selectedFood)

                    }


                }


            }



        }




    }

    private fun AddInList(i: DataSnapshot, product: String){

        storeName = i.child("name").value.toString()
        floor = i.child("floor").value.toString()
        price = i.child(product).value.toString()

        arrayList.add(
            Model(
                i.child("name").value.toString(),
                i.child("floor").value.toString(),
                i.child("image").value.toString(),
                i.child("product1").value.toString(),
                i.child("product2").value.toString(),
                i.child("product3").value.toString()
            )
        )


        myAdapter.notifyDataSetChanged()
        recyclerView.scrollToPosition(arrayList.size - 1)



    }

    private fun searchFood(food: String){


        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {


                for (i in p0.children) {


                    if (i.child("product1").value.toString() == food){

                        storeName = ""
                        floor = ""
                        selectedFood = ""
                        price = ""

                        AddInList(i, "product1price")
                    }

                    if (i.child("product2").value.toString() == food){
                        storeName = ""
                        floor = ""
                        selectedFood = ""
                        price = ""


                        AddInList(i, "product2price")
                    }

                    if (i.child("product3").value.toString() == food){
                        storeName = ""
                        floor = ""
                        selectedFood = ""
                        price = ""

                        AddInList(i, "product3price")
                    }


                }


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })

        }

    private fun deleteAll(){


            Chicken_Fries.isChecked = false
            Dinner_Baskets.isChecked = false
            Steak_and_Cheese.isChecked = false
            philadelphia_roll.isChecked = false
            Espresso.isChecked = false
            chicken_sandwiches.isChecked = false


    }





    class MyQiChatExecutor(qiContext: QiContext?) : BaseQiChatExecutor(qiContext) {





        override fun runWith(params: List<String>) {
            Log.i("TAG", "runWith")



            if (params.isNotEmpty()) {

                selectedFood = params[0]
                Log.i("159", selectedFood)


            }

        }




        override fun stop() {
            // This is called when chat is canceled or stopped.
            Log.i("TAG", "QiChatExecutor stopped")
        }

    }



    class MyQiChatExecutor1(qiContext: QiContext?) : BaseQiChatExecutor(qiContext) {


        override fun runWith(params: List<String>) {
            Log.i("TAG", "runWith")


            if (params.size > 0) {

                deletedFood = params[0]


            }

        }


        override fun stop() {
            // This is called when chat is canceled or stopped.
            Log.i("TAG", "QiChatExecutor stopped")
        }

    }

    private fun RemoveFromList(){


        val foodId = mapOf<String, CheckBox>(
            "Chicken Fries" to Chicken_Fries,
            "Dinner Baskets" to Dinner_Baskets,
            "Steak and Cheese" to Steak_and_Cheese,
            "philadelphia roll" to philadelphia_roll,
            "chicken sandwiches" to chicken_sandwiches,
            "Espresso" to Espresso
        )



            for (each in foodId.keys) {

                if (each == deletedFood) {

                    this.runOnUiThread {

                        Log.i("foodID", each)


                        foodId[each]?.isChecked = false


                }


            }

        }


            arrayList.forEach {

                if (deletedFood == it.product1 || deletedFood == it.product2 || deletedFood == it.product3) {

                    arrayList.remove(it)
                    Log.i("saxeli", "RemoveFromList: ")

            }

        }

    }

    private fun direct(title: String, floor: String, image: String){

        arrayList.clear()

        myAdapter.notifyDataSetChanged()

        val intent = Intent(this, MapActivity::class.java)
        intent.putExtra("extraTitle", title)
        intent.putExtra("extraFloor", floor)
        intent.putExtra("extraImage", image)

        startActivity(intent)

    }





    override fun onRobotFocusGained(qiContext: QiContext?) {


        backButtonSimpleToolbar.setOnClickListener{
            arrayList.clear()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()


        }

        val phrase: Phrase = Phrase("In this section you can choose the food you want to eat. Also I'll tell you store, you can eat that food and of course, the prise of that food.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()


        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.menu)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()



        submit.setOnClickListener {


            arrayList.clear()

            val foodId = mapOf<CheckBox, String>(
                Chicken_Fries to "Chicken Fries",
                Dinner_Baskets to "Dinner Baskets",
                Steak_and_Cheese to "Steak and Cheese",
                philadelphia_roll to "philadelphia roll",
                chicken_sandwiches to "chicken sandwiches",
                Espresso to "Espresso"
            )

            for (each in foodId.keys){


                if (each.isChecked){

                    searchFood(each.text.toString())


                }
                else{

                    arrayList.clear()
                    myAdapter.notifyDataSetChanged()

                }
            }
        }

        val executors = HashMap<String, QiChatExecutor>()

        // Map the executor name from the topic to our qiChatbotExecutor
        executors["selectedFood"] = MyQiChatExecutor(qiContext)
        executors["deletedFood"] = MyQiChatExecutor1(qiContext)
        executors["name"] = ShowMapClass(qiContext)

        // Set the executors to the qiChatbot
        chatbot.executors = executors




        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            Log.i("TAG", "bookmark")

            if (bookmark.name == "change") {

            Log.i("omiko", "selectedFood")

                val boolian = true
                foodSelection(selectedFood, boolian)

            }
            else if (bookmark.name == "delete") {


                Log.i("saxeli", deletedFood)

                RemoveFromList()



            }

            else if (bookmark.name == "all"){

                this.runOnUiThread {


                    deleteAll()

                    arrayList.clear()

                    myAdapter.notifyDataSetChanged()

                }

            }


            else if (bookmark.name == "INFO"){

//                    val editablePhraseSet = chatbot.dynamicConcept("storeName")
//
//                    phrases.clear()
//
//                    arrayList.forEach {
//
//                        Log.i("saxelebii", it.title)
//
//                        phrases.add(Phrase(it.title))
//                        phrases.add(Phrase(it.product1))
//                        phrases.add(Phrase(it.product2))
//                        phrases.add(Phrase(it.product3))
//
//                    }
//
//                    editablePhraseSet!!.addPhrases(phrases)
//                Log.i("achvene", phrases.toString())

                val booksDetailsVariable: QiChatVariable = chatbot!!.variable("store")
                booksDetailsVariable.value = storeName
                booksDetailsVariable.addOnValueChangedListener {
                    this.runOnUiThread {
                        Log.i("awd", "kiqna: ")
                    }


                }

                val floorName: QiChatVariable = chatbot!!.variable("floor")
                floorName.value = floor
                floorName.addOnValueChangedListener {
                    this.runOnUiThread {
                        Log.i("awd", "kiqna: ")
                    }


                }


                val priceName: QiChatVariable = chatbot!!.variable("price")
                priceName.value = price
                priceName.addOnValueChangedListener {

                }

            }
            else if (bookmark.name  == "back"){

                arrayList.clear()

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            }
            else if (bookmark.name  == "showMap"){


                arrayList.forEach{

                    if (selectedStore == it.title){

                        direct(it.title, it.des, it.image)
                    }

                }

            }
        }




        chat.async().run()


    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }

}