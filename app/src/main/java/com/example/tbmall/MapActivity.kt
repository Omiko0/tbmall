package com.example.tbmall

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.ActionBar
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.bumptech.glide.Glide
import com.example.tbmall.MenuActivity.Companion.arrayList
import com.example.tbmall.MenuActivity.Companion.myAdapter
import com.example.tbmall.SearchFoodActivity.Companion.arrayListmenu
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.row.view.*

class MapActivity : RobotActivity(), RobotLifecycleCallbacks {

    var title: String? = ""
    var floor: String? = ""
    var URL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

        val intent = intent

        title = intent.getStringExtra("extraTitle")
        floor = intent.getStringExtra("extraFloor")
        val image = intent.getStringExtra("extraImage")

        extraTitle.text = title
        extraFloor.text = floor
        Glide.with(this).load(image).into(extraImage)


    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }


    class WebClient internal constructor(private val activity: Activity): WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }

    private fun direction(){

        SearchFoodActivity.searchStoreNames.clear()
        SearchFoodActivity.arrayListmenu.clear()
        StoresActivity.storeArrayList.clear()
        ServicesActivity.servicesArrayList.clear()
        FoodActivity.foodArrayList.clear()
        arrayList.clear()


        val intentt = Intent(this, MainActivity::class.java)

        startActivity(intentt)

        finish()
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onRobotFocusGained(qiContext: QiContext?) {

        toBack.setOnClickListener{


            direction()

        }


        if (floor == "Floor : 1") {

            URL = "1_2"

        }
        else if (floor == "Floor : 2") {

            URL = "2_2"

        }
        else if (floor == "Floor : 3") {

            URL = "3_2"

        }
        else if (floor == "Floor : 4") {

            URL = "4_3"

        }

        else if (floor == "Floor : -1") {

            URL = "-1_3"

        }

        else if (floor == "Floor : 0") {

            URL = "0_1"

        }else{

            URL = "0_1"
        }

        this.runOnUiThread {

            web_view2.loadUrl("http://allgeorgia.com/pano/galleria/?startscene=fl$URL")
            web_view2.settings.javaScriptEnabled = true
            web_view2.canGoBack()
            web_view2.webViewClient = WebClient(this)

        }



        val phrase: Phrase = Phrase("Ok, on here you can tap my tablet and get exact direction of $title, but, first, don't forget to choose ${floor}")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.map)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()


        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            Log.i("TAG", "bookmark")

            if (bookmark.name == "back") {

                direction()
            }

        }



        chat.async().run()


    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}