package com.example.tbmall

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.CheckBox
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_menu.Chicken_Fries
import kotlinx.android.synthetic.main.activity_menu.Dinner_Baskets
import kotlinx.android.synthetic.main.activity_menu.Espresso
import kotlinx.android.synthetic.main.activity_menu.Steak_and_Cheese
import kotlinx.android.synthetic.main.activity_menu.chicken_sandwiches
import kotlinx.android.synthetic.main.activity_menu.philadelphia_roll
import kotlinx.android.synthetic.main.activity_menu.recyclerView
import kotlinx.android.synthetic.main.activity_menu.submit
import kotlinx.android.synthetic.main.activity_search_food.*
import kotlinx.android.synthetic.main.simpletoolbar.*

class SearchFoodActivity : RobotActivity(), RobotLifecycleCallbacks {

    companion object{
        var getFood: String = ""

        var searchStoreNames = mutableListOf<Phrase>()

        var storeName = ""
        var storeImage = ""
        var floor = ""
        var price = ""
        var deletedFoodName = ""

        var arrayListmenu = mutableListOf<Model>()
        lateinit var adapterButton: MyAdapter

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_food)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)


        adapterButton = MyAdapter(arrayListmenu, this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapterButton

    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private fun addInList(priceFood: String, i: DataSnapshot){

        storeName = i.child("name").value.toString()
        storeImage = i.child("image").value.toString()
        floor = "Floor : " + i.child("floor").value.toString()
        price = priceFood


        searchStoreNames.add(Phrase(i.child("name").value.toString()))


        arrayListmenu.add(
            Model(
                i.child("name").value.toString(),
                i.child("floor").value.toString(),
                i.child("image").value.toString(),
                i.child("product1").value.toString(),
                i.child("product2").value.toString(),
                i.child("product3").value.toString()

            )
        )

        adapterButton.notifyDataSetChanged()
        recyclerView.scrollToPosition(arrayListmenu.size - 1)


    }


    private fun showClick(food: String, chatbot: QiChatbot){


            val ref = FirebaseDatabase.getInstance().getReference("AllStores")

            ref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {

                    storeName = ""
                    storeImage = ""
                    floor = ""
                    price = ""


                    for (i in p0.children) {

                        if (food == i.child("product1").value.toString()) {


                            val priceFood = i.child("product1price").value.toString()
                            addInList(priceFood, i)

                        }
                        else if (food == i.child("product2").value.toString()) {


                            val priceFood = i.child("product2price").value.toString()
                            addInList(priceFood, i)

                        }

                        else if (food == i.child("product3").value.toString()) {


                            val priceFood = i.child("product3price").value.toString()
                            addInList(priceFood, i)

                        }

                    }


                }


                override fun onCancelled(p0: DatabaseError) {

                }


            })




    }

    private var searchFoodNames = mutableListOf<Phrase>()



    private fun addInMenuTopic(chatbot: QiChatbot){
        val ref = FirebaseDatabase.getInstance().getReference("AllStores")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                searchFoodNames.clear()


                for (i in p0.children) {

                    if (i.child("type").value.toString() == "kveba") {


                        searchFoodNames.add(Phrase(i.child("product1").value.toString()))
                        searchFoodNames.add(Phrase(i.child("product2").value.toString()))
                        searchFoodNames.add(Phrase(i.child("product3").value.toString()))


                    }

                }

                val thread = Thread(Runnable {


                    val editablePhraseSet = chatbot.dynamicConcept("foodName")



                    editablePhraseSet!!.addPhrases(searchFoodNames)


                })

                thread.start()


            }


            override fun onCancelled(p0: DatabaseError) {

            }


        })


    }

    private fun RemoveFromList(){

        Log.i("chemikai2", deletedFoodName)



        val foodId = mapOf<String, CheckBox>(
            "Chicken Fries" to Chicken_Fries,
            "Dinner Baskets" to Dinner_Baskets,
            "Steak and Cheese" to Steak_and_Cheese,
            "philadelphia roll" to philadelphia_roll,
            "chicken sandwiches" to chicken_sandwiches,
            "Espresso" to Espresso
        )



        for (each in foodId.keys) {

            if (each == deletedFoodName) {

                this.runOnUiThread {

                    Log.i("foodID", each)


                    foodId[each]?.isChecked = false


                }


            }

        }


        arrayListmenu.forEach {

            if (deletedFoodName == it.product1 || deletedFoodName == it.product2 || deletedFoodName == it.product3) {

                arrayListmenu.remove(it)
                Log.i("saxeli", "RemoveFromList: ")

            }

        }


    }

    private fun isChackedButton(food: String, chatbot: QiChatbot){

        val foodId = mapOf<String, CheckBox>(
            "Chicken Fries" to Chicken_Fries,
            "Dinner Baskets" to Dinner_Baskets,
            "Steak and Cheese" to Steak_and_Cheese,
            "philadelphia roll" to philadelphia_roll,
            "chicken sandwiches" to chicken_sandwiches,
            "Espresso" to Espresso
        )

            for(each in foodId.keys){

                if (each == food && foodId[each]?.isChecked == false){

                    this.runOnUiThread {

                        foodId[food]?.isChecked = true
                    }

                    showClick(food, chatbot)


                }
            }



    }


    private fun deleteAll(){



        arrayListmenu.clear()

        adapterButton.notifyDataSetChanged()

            Chicken_Fries.isChecked = false
            Dinner_Baskets.isChecked = false
            Steak_and_Cheese.isChecked = false
            philadelphia_roll.isChecked = false
            Espresso.isChecked = false
            chicken_sandwiches.isChecked = false

    }

    private fun directToMap(){

        val intent = Intent(this, MapActivity::class.java)

        intent.putExtra("extraTitle", storeName)
        intent.putExtra("extraImage", storeImage)
        intent.putExtra("extraFloor", floor)

        startActivity(intent)
    }



    override fun onRobotFocusGained(qiContext: QiContext?) {

        backButtonSimpleToolbar.setOnClickListener{

            arrayListmenu.clear()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()


        }



        val phrase: Phrase = Phrase("In this section you can choose the food you want to eat. Also I'll tell you store, you can eat that food and of course, the prise of that food.")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()


        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.searchfood)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        addInMenuTopic(chatbot)

        submit.setOnClickListener {

            arrayListmenu.clear()


            val foodId = mapOf<CheckBox, String>(
                Chicken_Fries to "Chicken Fries",
                Dinner_Baskets to "Dinner Baskets",
                Steak_and_Cheese to "Steak and Cheese",
                philadelphia_roll to "philadelphia roll",
                chicken_sandwiches to "chicken sandwiches",
                Espresso to "Espresso"
            )

            var foodText = ""

            for(each in foodId.keys){

                if (each.isChecked){

                    foodText = foodId[each].toString()


                    showClick(foodText, chatbot)



                }
                else{

                    arrayListmenu.clear()
                    adapterButton.notifyDataSetChanged()
                }

            }




        }


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        val executors = HashMap<String, QiChatExecutor>()

        executors["food"] = SearchFoodExecutor(qiContext)
        executors["deletedName"] = DeletedFoodClass(qiContext)



        chatbot.executors = executors

        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "showStore") {

                    isChackedButton(getFood, chatbot)

                val thread = Thread(Runnable {


                    val editablePhraseSetForStore = chatbot.dynamicConcept("storeName")



                    editablePhraseSetForStore!!.addPhrases(searchStoreNames)


                })

                thread.start()

            }
            else if(bookmark.name == "clearStores"){

                this.runOnUiThread {

                    deleteAll()

                }
            }

            else if (bookmark.name == "moreInfo"){

                Log.i("bookmarki", "onRobotFocusGained: ")

                val booksDetailsVariable: QiChatVariable = chatbot!!.variable("store")
                booksDetailsVariable.value = storeName
                booksDetailsVariable.addOnValueChangedListener {
                    this.runOnUiThread {
                        Log.i("awd", storeName)
                    }


                }

                val floorName: QiChatVariable = chatbot.variable("floor")
                floorName.value = floor
                floorName.addOnValueChangedListener {
                    this.runOnUiThread {
                        Log.i("awd", floor)
                    }


                }


                val priceName: QiChatVariable = chatbot.variable("price")
                priceName.value = price
                priceName.addOnValueChangedListener {

                    this.runOnUiThread {
                        Log.i("awd", price)
                    }

                }


            }
            else if(bookmark.name == "showDirection"){


                directToMap()


            }
            else if(bookmark.name == "delete"){

                RemoveFromList()

            }
            else if (bookmark.name == "goBack"){

                arrayListmenu.clear()

                val intent = Intent(this, MainActivity::class.java)

                startActivity(intent)

                finish()

            }

        }


        chat.async().run()




    }

    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }
}