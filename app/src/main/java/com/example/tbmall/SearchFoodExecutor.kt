package com.example.tbmall

import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor

import com.example.tbmall.SearchFoodActivity.Companion.getFood
import kotlin.math.log


class SearchFoodExecutor(qiContext: QiContext?) : BaseQiChatExecutor(
    qiContext
) {


    override fun runWith(params: List<String>) {


        if (params.size > 0) {


            getFood = params[0]

            Log.i("getfood", getFood)

        }

    }


    override fun stop() {

    }


}