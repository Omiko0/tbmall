package com.example.tbmall

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.actuation.Animate
import com.aldebaran.qi.sdk.`object`.actuation.Animation
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.`object`.human.ExcitementState
import com.aldebaran.qi.sdk.`object`.human.Gender
import com.aldebaran.qi.sdk.`object`.human.Human
import com.aldebaran.qi.sdk.`object`.humanawareness.HumanAwareness
import com.aldebaran.qi.sdk.builder.*
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_shopping.*

class MainActivity : RobotActivity(), RobotLifecycleCallbacks {

        var ageForData: Int = 0
        var genderForData: String = ""
        var exitementForData: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        QiSDK.register(this, this)

        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)


    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    // Store the HumanAwareness service.
    private var humanAwareness: HumanAwareness? = null

    // The QiContext provided by the QiSDK.
    private var qiContext: QiContext? = null


    private fun goStatisticActivity() {

        val statisticPage = Intent(this, StatisticActivity::class.java)

        startActivity(statisticPage)


    }

    private fun goToMenu(){

        val food = Intent(this, SearchFoodActivity::class.java)
        startActivity(food)

    }




    override fun onRobotFocusGained(qiContext: QiContext?) {

        StoreBtn.setOnClickListener {


            val shoppingPage = Intent(this, ShoppingActivity::class.java)
            startActivity(shoppingPage)

        }

        statistic.setOnClickListener {

            findHumansAround()

            goStatisticActivity()

        }

        goFoodBtn.setOnClickListener{

            goToMenu()

        }

        val phrase: Phrase = Phrase("Hello I'm Pepper, you can ask me things like, shopping, food and statistic. If you want me to tell you more about my functionality just ask me." +
                "  and i'll tell you everything")

        val say: Say = SayBuilder.with(qiContext)
            .withPhrase(phrase)
            .build()

        say.run()


        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.main)
            .build()

        val chatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()


        val chat: Chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()

        this.qiContext = qiContext

        humanAwareness = qiContext?.humanAwareness

        this.qiContext = null


        chatbot.addOnBookmarkReachedListener { bookmark: Bookmark ->

            if (bookmark.name == "goToShopping") {

                val shoppingPage = Intent(this, ShoppingActivity::class.java)
                startActivity(shoppingPage)

            }
            if (bookmark.name == "goToStatistic") {

                goStatisticActivity()


            }
            if (bookmark.name == "goToFood"){

                goToMenu()

            }
            if (bookmark.name == "hi"){

                val myAnimation: Animation = AnimationBuilder.with(qiContext)
                    .withResources(R.raw.hello_a002)
                    .build()

                val animate: Animate = AnimateBuilder.with(qiContext)
                    .withAnimation(myAnimation)
                    .build()

                animate.async().run()

            }

        }




        chat.async().run()



    }

    private fun findHumansAround() {
        val humansAroundFuture: Future<List<Human>>? = humanAwareness?.async()?.humansAround

        humansAroundFuture?.andThenConsume { humansAround ->
            Log.i("TAG", "${humansAround.size} human(s) around.")
            retrieveCharacteristics(humansAround)
        }

    }

    private fun retrieveCharacteristics(humans: List<Human>) {

        humans.forEach {

        }


        humans.forEachIndexed { index, human ->
            val age: Int = human.estimatedAge.years
            val gender: Gender = human.estimatedGender
            val excitementState: ExcitementState = human.emotion.excitement

            ageForData = age
            genderForData = gender.toString()
            exitementForData = excitementState.toString()

        }



        val ref = FirebaseDatabase.getInstance().getReference("TBData").push()


        val customerData = TBData()
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                customerData.age = ageForData
                customerData.gender = genderForData
                customerData.exitement = exitementForData
                ref.setValue(customerData)
                    .addOnSuccessListener {

                    }
                    .addOnFailureListener {

                    }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })


    }



    override fun onRobotFocusLost() {

    }

    override fun onRobotFocusRefused(reason: String?) {

    }


}